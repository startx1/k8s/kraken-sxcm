def stage(ctx):
    return {
        "parent": "root",
        "triggers": {
            "parent": True,
            "cron": "30 9 * * *",
            "repo": {
                "url": "https://gitlab.com/startx1/k8s/kraken-sxcm",
                "branch": "main",
                "interval": "15m"
            }
        },
        "parameters": [],
        "configs": [],
        "jobs": [{
            "name": "example job 1 DEVEL",
            "steps": [{
                "tool": "shell",
                "cmd": "echo 'example job 1 DEVEL'"
            }],
            "environments": [{
                "system": "any",
                "agents_group": "all",
                "config": "default"
            }]
        },{
            "name": "example job 2 DEVEL",
            "steps": [{
                "tool": "shell",
                "cmd": "echo 'example job 2 DEVEL'"
            }],
            "environments": [{
                "system": "any",
                "agents_group": "all",
                "config": "default"
            }]
        }]
    }
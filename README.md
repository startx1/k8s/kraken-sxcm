# kraken-sxcm

Kraken chaos scenarios for OKD/OCP cluster deployed using sxcm project. This is used with the [startx example-chaos helm chart](https://helm-repository.readthedocs.io/en/latest/charts/example-chaos/)

## Availables kraken tests

- [example stage](./example.py) with 2 examples jobs (echo a string)
- [helloworld stage](./helloworld.py) with 1 example job (echo a string)
